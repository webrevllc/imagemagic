myApp.factory('provConfig', function(){
    var service = [];
    service.materials = [
        {
            description: "Plastics",
            value: "plastic",
            urllabel: "Plastic",
            price: 3.5,
            printPrice: .25,
            engravePrice: 0
        },
        {
            description: "Metal",
            value: "metal",
            urllabel: "Metal",
            price: 5,
            printPrice: 0,
            engravePrice: 0
        },
        {
            description: "Wood",
            value: "wood",
            urllabel: "Wood",
            price: 7.5,
            printPrice: .25,
            engravePrice: 0
        }
    ];

    service.printOptions = [];
    service.printOptions.plastic = [];
    service.printOptions.metal = [];
    service.printOptions.wood = [];
    service.printOptions.plastic.printed =
        {
            //Printed
            value: "printed",
            label: "Printed",
            colors:
                [
                    {
                        label: "White",
                        face: 'white',
                        base: "white",
                        fill: "black",
                        price: 0
                    },
                    {
                        label: "White/Red",
                        face: "white",
                        base: "red",
                        fill: "#A51E1E",
                        price: 0
                    },
                    {
                        label: "White/Blue",
                        face: "white",
                        base: "blue",
                        fill: "#376B9B",
                        price: 0
                    },
                    {
                        label: "White/Black",
                        face: "white",
                        base: "black",
                        fill: "black",
                        price: 0
                    },
                    {
                        label: "Gold",
                        face: "gold",
                        base: "white",
                        fill: "white",
                        price: 0
                    },
                    {
                        label: "Silver",
                        face: "silver",
                        base: "white",
                        fill: "white",
                        price: 0
                    },
                    {
                        label: "Ivory",
                        face: "ivory",
                        base: "black",
                        fill: "black",
                        price: 0
                    },
                    {
                        label: "Almond",
                        face: "almond",
                        base: "black",
                        fill: "black",
                        price: 0
                    },
                    {
                        label: "Brushed Aluminum",
                        face: "brushed_aluminium",
                        base: "black",
                        fill: "black",
                        price: 0
                    }
                ]
        };

        service.printOptions.plastic.engraved =
        {
            //Engraved
            value: "engraved",
            label: "Engraved",
            price: 1,
            colors:
                [
                    {
                        label: "Dark Brown/White",
                        face: "dark_brown",
                        base: "white",
                        fill: "white",
                        price: 0
                    },
                    {
                        label: "Grey/White",
                        face: "grey",
                        base: "white",
                        fill: "white",
                        price: 0
                    },
                    {
                        label: "Bright Green/White",
                        face: "bright_green",
                        base: "white",
                        fill: "white",
                        price: 0
                    },
                    {
                        label: "Orange/White",
                        face: "orange",
                        base: "white",
                        fill: "white",
                        price: 0
                    },
                    {
                        label: "White/Black",
                        face: "white",
                        base: "black",
                        fill: "black",
                        price: 0
                    },
                    {
                        label: "Red/White",
                        face: "red",
                        base: "white",
                        fill: "white",
                        price: 0
                    },
                    {
                        label: "White/Blue",
                        face: "white",
                        base: "blue",
                        fill: "#376B9B",
                        price: 0
                    },
                    {
                        label: "Black/White",
                        face: "black",
                        base: "white",
                        fill: "white",
                        price: 0
                    },
                    {
                        label: "Blue/White",
                        face: "blue",
                        base: "white",
                        fill: "white",
                        price: 0
                    },
                    {
                        label: "Yellow/Black",
                        face: "yellow",
                        base: "black",
                        fill: "black",
                        price: 0
                    },
                    {
                        label: "Almond/Black",
                        face: "almond",
                        base: "black",
                        fill: "black",
                        price: 0
                    },
                    {
                        label: "White/Red",
                        face: "white",
                        base: "red",
                        fill: "#A51E1E",
                        price: 0
                    },
                    {
                        label: "Forest Green/White",
                        face: "forrest_pine_green",
                        base: "white",
                        fill: "white",
                        price: 0
                    },
                    {
                        label: "Maroon/White",
                        face: "maroon",
                        base: "white",
                        fill: "white",
                        price: 0
                    },
                    {
                        label: "Sapphire/White",
                        face: "sapphire",
                        base: "white",
                        fill: "white",
                        price: 0
                    },

                    {
                        label: "Navy Blue/White",
                        face: "navy",
                        base: "white",
                        fill: "white",
                        price: 0
                    },
                    {
                        label: "Purple/White",
                        face: "purple",
                        base: "white",
                        fill: "white",
                        price: 0
                    },
                    {
                        label: "Celadon/White",
                        face: "celadon",
                        base: "white",
                        fill: "white",
                        price: 0
                    },
                    {
                        label: "Matte Black/Silver",
                        face: "black",
                        base: "silver",
                        fill: "silver",
                        price: 0
                    },
                    {
                        label: "Light Blue/White",
                        face: "light_blue",
                        base: "white",
                        fill: "white",
                        price: 0
                    },
                    {
                        label: "European Gold/Black",
                        face: "european_gold",
                        base: "black",
                        fill: "black",
                        price: .15
                    },
                    {
                        label: "Brushed Aluminum/Black",
                        face: "brushed_aluminium",
                        base: "black",
                        fill: "black",
                        price: .15
                    },
                    {
                        label: "Radiant Gold/Black",
                        face: "radiant_gold",
                        base: "black",
                        fill: "black",
                        price: .15
                    },
                    {
                        label: "Smooth Silver/Black",
                        face: "smooth_silver",
                        base: "black",
                        fill: "black",
                        price: .15
                    },
                    {
                        label: "Black/Gold",
                        face: "black",
                        base: "gold",
                        fill: "#9F834F",
                        price: .15
                    },
                    {
                        label: "Navy/Gold",
                        face: "navy",
                        base: "gold",
                        fill: "#9F834F",
                        price: .15
                    },
                    {
                        label: "Red/Gold",
                        face: "red",
                        base: "gold",
                        fill: "#9F834F",
                        price: .15
                    },
                    {
                        label: "Pine Green/Gold",
                        face: "forrest_pine_green",
                        base: "gold",
                        fill: "#9F834F",
                        price: .15
                    }

                ]
        };

    service.printOptions.metal.engraved = {
        value: "engraved",
        label: "Engraved",
        price: 0,
        colors:
            [
                {
                    label: "Bright Gold",
                    face: "bright_gold",
                    base: "gold",
                    fill: "#000",
                    price: 0
                },
                {
                    label: "Satin Gold",
                    face: "satin_gold",
                    base: "gold",
                    fill: "#000",
                    price: 0
                },
                {
                    label: "Bright Silver",
                    face: "bright_silver",
                    base: "silver",
                    fill: "#000",
                    price: 0
                },
                {
                    label: "Satin Silver",
                    face: "satin_silver",
                    base: "silver",
                    fill: "#000",
                    price: 0
                },
                {
                    label: "Chrome Plated",
                    face: "chrome_plated",
                    base: "silver",
                    fill: "#000",
                    price: 0
                }
            ]
    };
    service.printOptions.wood.engraved = {
        value: "engraved",
        label: "Engraved",
        price: 0,
        colors:
            [
                {
                    label: "Walnut",
                    face: "walnut",
                    base: "walnut",
                    fill: "#000",
                    price: 0
                },
                {
                    label: "Cherry",
                    face: "cherry",
                    base: "cherry",
                    fill: "#000",
                    price: 0
                },
                {
                    label: "Maple",
                    face: "maple",
                    base: "maple",
                    fill: "#000",
                    price: 0
                },
                {
                    label: "Birch",
                    face: "birch",
                    base: "birch",
                    fill: "#000",
                    price: 0
                },
                {
                    label: "Cedar",
                    face: "cedar",
                    base: "cedar",
                    fill: "#000",
                    price: 0
                },
                {
                    label: "Bamboo",
                    face: "bamboo",
                    base: "bamboo",
                    fill: "#000",
                    price: 0
                }
            ]
    };

        service.sizes = [
            {
                label: "1\" x 3\"",
                value: "1-3",
                base: "3",
                tall: "1",
                backingOffset: 100,
                yOffset: 500,
                xOffset: 660,
                x: 218,
                y: 450,
                width: 1307,
                height: 440,
                radius: 65,
                imageX: 250,
                imageY: 360,
                imageTopY: 240,
                imageMiddleY: 540,
                imageBottomY: 540,
                imageHeight: 185,
                imageWidth: 190
            },
            {
                label: "1 1/4\" x 3\"",
                value: "125-325",
                base: "3",
                backingOffset: 80,
                tall: "125",
                yOffset: 475,
                xOffset: 680,
                x: 218,
                y: 450,
                width: 1307,
                height: 550,
                radius: 65,
                imageX: 250,
                imageY: 320,
                imageTopY: 540,
                imageMiddleY: 540,
                imageBottomY: 540,
                imageHeight: 185,
                imageWidth: 190
            },
            {
                label: "1 1/2\" x 3\"",
                value: "15-3",
                base: "3",
                tall: "15",
                yOffset: 360,
                xOffset: 650,
                x: 218,
                y: 308,
                width: 1307,
                height: 654,
                radius: 65,
                imageX: 250,
                imageY: 300,
                imageTopY: 540,
                imageMiddleY: 540,
                imageBottomY: 540,
                imageHeight: 185,
                imageWidth: 190


            },
            {
                label: "2\" x 3\"",
                value: "2-3",
                base: "3",
                tall: "2",
                yOffset: 250,
                xOffset: 650,
                x: 218,
                y: 165,
                width: 1307,
                height: 869,
                radius: 65,
                imageX: 250,
                imageY: 230,
                imageTopY: 540,
                imageMiddleY: 540,
                imageBottomY: 540,
                imageHeight: 185,
                imageWidth: 190
            },
            {
                label: "3/4\" x 3\"",
                value: "75-3",
                base: "3",
                backingOffset: 135,
                tall: "75",
                yOffset: 450,
                xOffset: 680,
                x: 218,
                y: 368,
                width: 1307,
                height: 330,
                radius: 65,
                imageX: 250,
                imageY: 400,
                imageTopY: 540,
                imageMiddleY: 540,
                imageBottomY: 540,
                imageHeight: 185,
                imageWidth: 190
            },
            {
                label: "1/2\" x 2 3/8\"",
                value: "5-2.375",
                base: "2.375",
                backingOffset: 160,
                tall: "5",
                yOffset: 650,
                xOffset: 670,
                x: 308,
                y: 595,
                width: 1028,
                height: 215.38,
                radius: 60,
                imageX: 350,
                imageY: 465,
                imageTopY: 540,
                imageMiddleY: 540,
                imageBottomY: 540,
                imageHeight: 120,
                imageWidth: 120
            }
        ];

    service.corners = [
        {
            label: "Square Edge",
            value: "square",
            rounded: 0
        },
        {
            label: "3/16\" Round",
            value: "round_.1875",
            rounded: 48.2625
        },
        {
            label: "Beveled",
            value: "bevel",
            rounded: 0
        },
        {
            label: "1/4\" Round",
            value: "round_.25",
            rounded: 64.35
        },
        {
            label: "1/4\" Notched",
            value: "notch_.25",
            rounded: 'nan',
            disabled: ''
        }
    ];

    service.backings =
        [
            {
                "label": "Pin",
                "url": "pin",
                "price": 0
            },
            {
                "label": "Magnet",
                "url": "magnet",
                "price": 1.5
            },
            {
                "label": "Double Clutch",
                "url": "double-clutch",
                "price": 0
            },
            {
                "label": "Pocket Attachment",
                "url": "pocket",
                "price": 1.5
            },
            {
                "label": "Swivel Bulldog Clip",
                "url": "clip",
                "price": .45
            },
            {
                "label": "Slot Punch",
                "url": "slot",
                "price": .1
            },
            {
                "label": "Double Sided Adhesive",
                "url": "double_tape",
                "price": .25
            },
            {
                "label": "No Backing",
                "url": "none",
                "price": 0
            }
        ];
    return service;
});

