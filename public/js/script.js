
var myApp = angular.module('providence', ['LocalStorageModule'], function($locationProvider, $interpolateProvider){
    $locationProvider.html5Mode({
        enabled: true
    });

    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
});

myApp.config(function(localStorageServiceProvider){
    localStorageServiceProvider
        .setPrefix('PROVIDENCE');
});

myApp.controller('svgFront', function(provConfig,  $http, $scope, $sce, localStorageService, $location) {

    var ctrl = this;
    ctrl.pId = $location.path().split("/")[2]||"Unknown";

    //INITIAL CONFIGS
    ctrl.config = [];
    ctrl.text = "[Line 1]";
    ctrl.materials = provConfig.materials;
    ctrl.printOptions = provConfig.printOptions;
    ctrl.config.selectedPrintOption = provConfig.printOptions['plastic']['printed'];
    // console.log(provConfig.printOptions.metal.engraved.colors);
    ctrl.selectedMaterial = 0;
    // ctrl.selectedPrintOption = 0;
    ctrl.corners = provConfig.corners;

//        ctrl.config.selectedPrintOption = provConfig.printOptions[0]

    ctrl.backingOptions = provConfig.backings;

    ctrl.dismissAndDelete = function () {
        localStorageService.clearAll();
        location.reload();
    };



    ctrl.setPortable = function (key, value) {
        ctrl.portable[key] = value;
        // console.log(ctrl.portable);
        localStorageService.set('config', ctrl.portable);
    };

    ctrl.config.numlines =
        [
            {
                'size': 0,
                'horizontalAlign': 'middle',
                'font': 'Calibri',
                'color': 'black',
                'description': "No Text"
            },
            {
                'size': 58,
                'horizontalAlign': 'middle',
                'font': 'Calibri',
                'color': 'black',
                "price": .5,
                'description': "1 Line"
            },
            {
                'size': 58,
                'horizontalAlign': 'middle',
                'font': 'Calibri',
                'color': 'black',
                "price": 1,
                'description': "2 Lines"
            },
            {
                'size': 58,
                'horizontalAlign': 'middle',
                'font': 'Calibri',
                'color': 'black',
                "price": 1.5,
                'description': "3 Lines"
            },
            {
                'size': 58,
                'horizontalAlign': 'middle',
                'font': 'Calibri',
                'color': 'black',
                "price": 2,
                'description': "4 Lines"
            }
        ];
    ctrl.linesSelected = 2;


    ctrl.sizes = provConfig.sizes;
    ctrl.selectedPage = 'material';
    ctrl.valign = true;
    //SETTERS

    ctrl.repeat = function (num) {
        var retval = [];
        for (var i = 0; i <= num; i++) {
            retval.push(i);
        }
        return retval;
    };

    ctrl.getNumber = function(num) {
        return new Array(num);
    };

    ctrl.isBevel = function () {
        return ctrl.config.selectedCorner.value == 'bevel';
    };
    ctrl.setMaterial = function (i) {
        ctrl.selectedMaterial = i;
        if (i == 0) {
            ctrl.setCorner(ctrl.corners[3]);
            ctrl.setColor(provConfig.printOptions.plastic.printed.colors[0]);
            ctrl.setPrintOption(provConfig.printOptions['plastic']['printed'], 0);
            if(ctrl.portable.logoUrl){
                ctrl.logoUrl = ctrl.portable.colorUrl;
            }
        }
        if (i == 1) {
            ctrl.setCorner(ctrl.corners[3]);
            ctrl.setColor(provConfig.printOptions.metal.engraved.colors[0]);
            ctrl.setPrintOption(provConfig.printOptions['metal']['engraved'], 1);
        }
        if (i == 2) {
            ctrl.setCorner(ctrl.corners[3]);
            ctrl.setColor(provConfig.printOptions.wood.engraved.colors[0]);
            ctrl.setPrintOption(provConfig.printOptions['wood']['engraved'], 1);
        }
        ctrl.config.selectedMaterial = provConfig.materials[i];
        ctrl.setPortable('selectedMaterial', ctrl.config.selectedMaterial);
    };

    ctrl.setOption = function (i) {
        ctrl.selectedPrintOption = i;
        ctrl.selectedPrint = '';
        switch (i) {
            case 0:
                ctrl.selectedPrint = "PRINTED";
                if(ctrl.portable.colorUrl){
                    ctrl.logoUrl = ctrl.portable.colorUrl;
                    ctrl.setPortable('logoUrl', ctrl.portable.colorUrl);
                }
                break;
            case 1:
                ctrl.selectedPrint = "ENGRAVED";
                if(ctrl.portable.colorUrl){
                    ctrl.logoUrl = ctrl.portable.blackUrl;
                    ctrl.setPortable('logoUrl', ctrl.portable.blackUrl);
                }
                break;
        }
        ctrl.config.selectedPrintOption = provConfig.printOptions[ctrl.config.selectedMaterial.value][(ctrl.selectedPrint).toLowerCase()];

        ctrl.setPortable('selectedPrintOption', ctrl.config.selectedPrintOption);
        // console.log(ctrl.portable);
    };



    ctrl.setSize = function (size) {
        if (size.value == "5-2.375" || size.value == "75-3") {
            ctrl.corners[4].disabled = true;
        }
        ctrl.config.selectedSize = size;
        ctrl.setPortable('selectedSize', size);
    };

    ctrl.setCorner = function (corner) {
        ctrl.config.selectedCorner = corner;
        ctrl.setPortable('selectedCorner', ctrl.config.selectedCorner);
    };
    ctrl.setColor = function (color) {
        ctrl.config.selectedColor = color;
        ctrl.setPortable('selectedColor', ctrl.config.selectedColor);
    };
    ctrl.setPrintOption = function (printOption, value) {
        ctrl.config.selectedPrintOption = printOption;
        ctrl.selectedPrintOption = value;
        ctrl.setPortable('selectedPrintOption', value);
    };

    //GETTERS
    ctrl.getMaterial = function () {
        return ctrl.config.selectedMaterial;
    };
    ctrl.getSize = function () {
        return ctrl.config.selectedSize;
    };
    ctrl.getCorner = function () {
        return ctrl.config.selectedCorner.value;
    };
    ctrl.getColor = function () {
        ctrl.getLogoURL();
        return ctrl.config.selectedColor;
    };

    ctrl.logoUrl = '';
    ctrl.getLogoURL = function () {
        var url = '';
        if (ctrl.logoUrl == '') {
            switch (ctrl.config.selectedColor.base) {
                case "gold":
                    url = '/images/5_gold_Logo_Here-01.png';
                    break;
                case "yellow":
                    url = '/images/5_yellow_Logo_Here-01.png';
                    break;
                case "red":
                    url = '/images/5_red_Logo_Here-01.png';
                    break;
                case "blue":
                    url = '/images/5_blue_Logo_Here-01.png';
                    break;
                case "black":
                    url = '/images/Logo_Here-01.png';
                    break;
                case "silver":
                    url = '/images/5_silver_Logo_Here-01.png';
                    break;
                case "white":
                    url = '/images/5_white_Logo_Here-01.png';
                    break;
            }
            if (ctrl.config.selectedColor.face == "white" && ctrl.config.selectedColor.base == "white")
                url = '/images/Logo_Here-01.png';
        } else {
            url = $sce.trustAsResourceUrl(ctrl.logoUrl);
        }

        return url;
    };

    ctrl.dpi = .65;


    ctrl.getLogoHeight = function () {
        var height = (ctrl.myNametag.logo.scaleWidth * ctrl.dpi) / ctrl.myNametag.logo.width * ctrl.myNametag.logo.height;
        var maxHeight = (ctrl.config.selectedSize.height - .1) * ctrl.dpi;
        if (height > maxHeight) {
            ctrl.myNametag.logo.scaleWidth = maxHeight / ctrl.dpi * ctrl.myNametag.logo.width / ctrl.myNametag.logo.height;
            height = (ctrl.myNametag.logo.scaleWidth * ctrl.dpi) / ctrl.myNametag.logo.width * ctrl.myNametag.logo.height;
        }
        return height;
    };

    ctrl.getLogoWidth = function () {
        if (ctrl.myNametag.logo.scaleWidth > ctrl.config.selectedSize.width - .1)
            ctrl.myNametag.logo.scaleWidth = ctrl.config.selectedSize.width - .1;
        return ctrl.myNametag.logo.scaleWidth * ctrl.dpi;
    };

    ctrl.imageSizeChange = function (change) {
        ctrl.myNametag.logo.scaleWidth += change;
        if (ctrl.myNametag.logo.scaleWidth <= .1)
            ctrl.myNametag.logo.scaleWidth = .1;
        if (ctrl.myNametag.logo.scaleWidth > ctrl.config.selectedSize.width - .1)
            ctrl.myNametag.logo.scaleWidth = ctrl.config.selectedSize.width - .1;

        ctrl.setPortable('logoWidth', ctrl.myNametag.logo.scaleWidth);
        // ctrl.setPortable('logoHeight', ctrl.getLogoHeight());
        // ctrl.setPortable('scaleWidth', ctrl.myNametag.logo.scaleWidth);
    };


    ctrl.changeNav = function (page) {
        ctrl.selectedPage = page;
    };

    ctrl.hasImage = true;
    ctrl.imageSwitch = function () {
        return !hasImage;
    };

    ctrl.ntHeight = 736;
    ctrl.ntWidth = 1308;

    ctrl.getXShift = function () {
        return (ctrl.ntWidth - (ctrl.config.selectedSize.width * ctrl.dpi )) / 2;
    };

    ctrl.getYShift = function () {
        return ctrl.ntHeight - (ctrl.config.selectedSize.height * ctrl.dpi) + 4;
    };

    ctrl.changeLineSpacing = function (index, amount) {
        ctrl.getLineConfig(index).spacing += amount;
    };
    ctrl.changeLineFontSize = function (index, amount) {
        ctrl.getLineConfig(index).size += amount;
    };


    ctrl.Nametag = function () {
        //**ShoppingCartItem Required Properties	**//
        this.qty = 1;
        this.name = "My Nametag";
        //**										**//
        this.contrast = 5;
        this.padding = 30;
        // this.config = NAMETAG_CONFIG;

        this.textJustification = "middle";

        this.logo = {
            "path": "images/",
            "image": "Logo_Here-01.png",
            "width": 860,
            "height": 981,
            "scaleWidth": 258,
            "verticalMargin": 0,
            "horizontalMargin": 0,
            "file": {response: {}}
        };

        this.background = {
            "path": "/images/",
            "image": "default-BG.png",
            "width": 1530,
            "height": 1000,
            "scaleWidth": 0,
            "verticalMargin": 0,
            "horizontalMargin": 0,
            "file": {response: {}}
        };

        this.textBoxAlignment = {vertical: 0, horizontal: 0};
        this.lineConfig = [
            {
                'size': 68,
                'spacing': 20,
                'horizontalAlign': 'middle',
                'font': 'Calibri',
                'color': 'black'
            },
            {
                'size': 38,
                'spacing': 20,
                'horizontalAlign': 'middle',
                'font': 'Calibri',
                'color': 'black'
            },
            {
                'size': 38,
                'spacing': 20,
                'horizontalAlign': 'middle',
                'font': 'Calibri',
                'color': 'black'
            },
            {
                'size': 38,
                'spacing': 20,
                'horizontalAlign': 'middle',
                'font': 'Calibri',
                'color': 'black'
            }
        ];  //Line Configuration
        this.names = [];  //Name List
        this.names.push([]);
        this.linePosition = {"x": 200, "y": 0, "height": 200, "width": 300};
        return this;
    };

    ctrl.getEngravedLogoColor = function () {
        if (ctrl.myNametag.logo.image != "spinner.gif" && ctrl.selectedPrint == "ENGRAVED") {
            var NTType = ctrl.getColorQuestionCode();
            //Get the color group associated with the question code
            var myColor = ctrl.getSelectedAnswerByQuestionCode(ctrl.getColorQuestionCode());
            var color = (myColor.engPicture ? myColor.engPicture : myColor.backColor).replace(/ /g, "_").toLowerCase();
            return ctrl.myNametag.contrast + "_" + color + "_";
        }
        return "";
    };

    ctrl.setBackgroundOption = function(){
        ctrl.setBackground();
        ctrl.setPortable('isFullBleed', ctrl.isFullBleed);
    };
    ctrl.setBackground = function(){
        ctrl.isFullBleed = !ctrl.isFullBleed;
    };

    ctrl.updateBackgroundColor = function(){
        ctrl.setPortable('backgroundColor', ctrl.myNametag.background.color);
    };

    ctrl.getBackgroundHeight = function () {
        if (ctrl.myNametag.background.image == "spinner.gif")
            return 50000;
        return (ctrl.config.selectedSize.width) * (ctrl.myNametag.background.height / ctrl.myNametag.background.width) * ctrl.dpi;
        // return (300);
    };

    ctrl.getBackgroundWidth = function () {
        if (ctrl.myNametag.background.image == "spinner.gif")
            return 50000;
        return (ctrl.config.selectedSize.width + .25) * ctrl.dpi;
    };

    ctrl.getBackgroundXPosition = function () {
        var nametagWidth = ctrl.config.selectedSize.width * ctrl.dpi;
        if (ctrl.myNametag.background.image == "spinner.gif")
            return ctrl.getXShift() - 100 + (nametagWidth / 2);

        var horizontalMargin = ctrl.myNametag.background.horizontalMargin;
        return ctrl.getXShift() + horizontalMargin - (.125 * ctrl.dpi);
    };

    ctrl.getBackgroundYPosition = function () {
        var verticalMargin = ctrl.myNametag.background.verticalMargin;
        var nametagHeight = ctrl.config.selectedSize.height * ctrl.dpi;
        if (ctrl.myNametag.background.image == "spinner.gif")
            return ctrl.getYShift() - (100) + (nametagHeight / 2);


        return ctrl.getYShift() - (ctrl.getBackgroundHeight() / 2) + (nametagHeight / 2) + verticalMargin + 25;
    };

    ctrl.isEngraved = function () {
        //0 is printed, 1 is engraved
        return ctrl.selectedPrintOption == 1;
    };

    ctrl.myNametag = ctrl.Nametag();

    ctrl.getLineConfig = function (index) {
        return ctrl.myNametag.lineConfig[index];
    };
    ctrl.getFontColorForLine = function () {
        return ctrl.config.selectedColor.fill;
    };

    ctrl.toggleHAlignment = function (alignment) {
        ctrl.myNametag.logoHorizontalAlignment = alignment;
        ctrl.halign = alignment;
        ctrl.setPortable('logoHorizontalAlignment', alignment);
    };
    ctrl.toggleVAlignment = function (alignment) {
        ctrl.myNametag.logoVerticalAlignment = alignment;
        ctrl.setPortable('logoVerticalAlignment', alignment);
    };
    ctrl.getXTextPosition = function (i) {
        var Horizontal = (ctrl.myNametag.logoHorizontalAlignment == null ? "left" : ctrl.myNametag.logoHorizontalAlignment);
        var Vertical = (ctrl.myNametag.logoVerticalAlignment == null ? "middle" : ctrl.myNametag.logoVerticalAlignment);
        var logoWidth = (!ctrl.hasImage ? 0 : ctrl.myNametag.logo.scaleWidth * ctrl.dpi + ctrl.myNametag.padding);
        if (Horizontal == "center") {
            logoWidth = 0;
        }
        var nametagWidth = ctrl.config.selectedSize.width * ctrl.dpi;
        var config = ctrl.getLineConfig(i);

        var Justify = 40;
        if (config.horizontalAlign == "middle")
            Justify = (nametagWidth - logoWidth) / 2;
        if (config.horizontalAlign == "end")
            Justify = (nametagWidth - ctrl.myNametag.padding - logoWidth);
        if (Horizontal == "right") {
            return ctrl.getXShift() + Justify + ctrl.myNametag.textBoxAlignment.horizontal;
        }


        return ctrl.getXShift() + logoWidth + Justify + ctrl.myNametag.textBoxAlignment.horizontal;
    };

    ctrl.getYTextPosition = function (index) {
        var totalHeight = 0;
        for (var i = 0; i < ctrl.linesSelected; i++) {
            var line = ctrl.getLineConfig(i);
            totalHeight += line.size + line.spacing;
        }

        var nametagHeight = ctrl.config.selectedSize.height * ctrl.dpi;


        var Vertical = (ctrl.myNametag.logoVerticalAlignment == null ? "middle" : ctrl.myNametag.logoVerticalAlignment);

        var Horizontal = (ctrl.myNametag.logoHorizontalAlignment == null ? "left" : ctrl.myNametag.logoHorizontalAlignment);

        var centerHeight = nametagHeight / 2 - totalHeight / 2;

        if (Vertical == "top" && Horizontal == "center")
            centerHeight = ctrl.getLogoHeight();
        if (Vertical == "bottom" && Horizontal == "center")
            centerHeight = 0;
        if (Vertical == "middle" && Horizontal == "center")
            centerHeight = 0;

        var y = 0;
        for (var i = 0; i <= index; i++) {
            var line = ctrl.getLineConfig(i);
            y += line.size + line.spacing;
        }

        return y + ctrl.getYShift() + ctrl.myNametag.textBoxAlignment.vertical + centerHeight;
    };


    ctrl.getLogoXPosition = function () {
        var horizontalMargin = ctrl.myNametag.logo.horizontalMargin;
        var Horizontal = (ctrl.myNametag.logoHorizontalAlignment == null ? "left" : ctrl.myNametag.logoHorizontalAlignment);
        var nametagWidth = ctrl.config.selectedSize.width * ctrl.dpi;
        var logoWidth = (!ctrl.hasImage ? 0 : ctrl.myNametag.logo.scaleWidth * ctrl.dpi + ctrl.myNametag.padding);


        if (Horizontal == "left") {
            ctrl.halign = Horizontal;
            return ctrl.getXShift() + ctrl.myNametag.padding + horizontalMargin;
        }
        else if (Horizontal == "right") {
            ctrl.halign = Horizontal;
            return ctrl.getXShift() + nametagWidth - logoWidth - ctrl.myNametag.padding + horizontalMargin;
        }
        else if (Horizontal == "center") {
            ctrl.halign = Horizontal;
            return ctrl.getXShift() + nametagWidth / 2 - logoWidth / 2 + horizontalMargin;
        }
    };

    ctrl.getLogoYPosition = function () {
        var Vertical = (ctrl.myNametag.logoVerticalAlignment == null ? "middle" : ctrl.myNametag.logoVerticalAlignment);
        var nametagHeight = ctrl.config.selectedSize.height * ctrl.dpi;
        var logoHeight = (!ctrl.hasImage ? 0 : (ctrl.myNametag.logo.scaleWidth * ctrl.dpi) / ctrl.myNametag.logo.width * ctrl.myNametag.logo.height);

        var verticalMargin = ctrl.myNametag.logo.verticalMargin;
        if (Vertical == "top") {
            ctrl.valign = Vertical;
            return ctrl.getYShift() + verticalMargin;
        }
        else if (Vertical == "middle") {
            ctrl.valign = Vertical;
            return ctrl.getYShift() + (nametagHeight / 2) - (logoHeight / 2) + verticalMargin;
        }
        else if (Vertical == "bottom") {
            ctrl.valign = Vertical;

            return ctrl.getYShift() + nametagHeight - logoHeight + verticalMargin;
        }

    };


    ctrl.setTextHorizontal = function (val) {
        angular.forEach(ctrl.myNametag.lineConfig, function (line, i) {
            line.horizontalAlign = val;
        });
    };

    ctrl.updateVerticalMargin = function (logo, amount) {
        logo.verticalMargin += amount;
    };

    ctrl.updateHorizontalMargin = function (logo, amount) {
        logo.horizontalMargin += amount;
    };

    ctrl.updateTextVerticalMargin = function (amount) {
        ctrl.myNametag.textBoxAlignment.vertical += amount;
    };

    ctrl.updateTextHorizontalMargin = function (amount) {
        ctrl.myNametag.textBoxAlignment.horizontal += amount;
    };


    $scope.uploadFile = function (files) {
        var fd = new FormData();
        //Take the first selected file
        fd.append("file", files[0]);
        $http.post('http://localhost:8000/image-color', fd, {
            withCredentials: false,
            headers: {'Content-Type': undefined},
            transformRequest: angular.identity
        })
            .success(function (data) {
                var filename = data;
                ctrl.newLogo = true;
                ctrl.logoUrl = 'http://imagemagic.webrevllc.com/uploads/logos/' + filename;
            })
            .error(function () {
                console.log('hmmm')
            });
    };

    $scope.uploadBGFile = function (files) {
        var fd = new FormData();
        //Take the first selected file
        fd.append("file", files[0]);

        $http.post('http://imagemagic.webrevllc.com/bg', fd, {

            withCredentials: false,
            headers: {'Content-Type': undefined},
            transformRequest: angular.identity
        })
            .success(function (data) {
                var filename = data;
                ctrl.newBG = true;
                ctrl.myNametag.background.image = 'http://imagemagic.webrevllc.com/uploads/bg/' + filename;
                ctrl.myNametag.background.path = '';
            })
            .error(function () {
                console.log('hmmm')
            });
    };

    ctrl.getBackgroundUrl = function () {
        if (ctrl.newBG) {
            return $sce.trustAsResourceUrl(ctrl.myNametag.background.image);
        } else {
            return ctrl.myNametag.background.path + ctrl.myNametag.background.image;
        }

    };
    ctrl.myNametag.lineConfig[0].horizontalAlign = 'middle';

    ctrl.logoCallback = {
        'addedfile': function (file) {
        },
        'success': function (file, xhr) {
            ctrl.newLogo = true;
            ctrl.logoUrl = ctrl.portable.selectedPrintOption == 0 ? xhr[2] : xhr[3];
            ctrl.myNametag.logo.height = xhr[0];
            ctrl.nameTagID = xhr[1];
            ctrl.setPortable('nameTagID', ctrl.nameTagID);
            ctrl.setPortable('scaleWidth', ctrl.myNametag.logo.scaleWidth);
            ctrl.setPortable('logoHeight', ctrl.myNametag.logo.height);
            ctrl.setPortable('colorUrl', xhr[2]);
            ctrl.setPortable('blackUrl', xhr[3]);
            ctrl.setPortable('logoUrl', ctrl.logoUrl);
            ctrl.hasIDLogo.url = 'http://localhost:8000/nametag/' + ctrl.nameTagID + '/logo';
            ctrl.hasIDBackgroundImageOptions.url = 'http://localhost:8000/nametag/' + ctrl.nameTagID + '/bg';
            ctrl.hasIDCsvUploader.url = 'http://localhost:8000/nametag/' + ctrl.nameTagID + '/csv';
            $('#logo').modal('hide');
        }

    };

    ctrl.backgroundCallback = {
        'addedfile': function (file) {
        },
        'success': function (file, xhr) {
            var filename = xhr[0];
            ctrl.nameTagID = xhr[1];
            ctrl.newBG = true;
            ctrl.myNametag.background.image = filename;
            ctrl.setPortable('backgroundURL', filename);
            ctrl.setPortable('nameTagID', ctrl.nameTagID);
            ctrl.myNametag.background.path = '';
            ctrl.hasIDLogo.url = 'http://localhost:8000/nametag/' + ctrl.nameTagID + '/logo';
            ctrl.hasIDBackgroundImageOptions.url = 'http://localhost:8000/nametag/' + ctrl.nameTagID + '/bg';
            ctrl.hasIDCsvUploader.url = 'http://localhost:8000/nametag/' + ctrl.nameTagID + '/csv';
            $('#background').modal('hide');
        }

    };

    ctrl.csvCallback = {
        'addedfile': function (file) {
            ctrl.uploading = true;
        },
        'success': function (file, xhr) {
            console.log(xhr);
            ctrl.uploading = false;
            ctrl.hasCSV = true;
            ctrl.myNametag.names = xhr.results;
            // console.log(xhr);
            ctrl.qty = parseInt(xhr.qty);
            ctrl.linesSelected = parseInt(xhr.lines);
            ctrl.nameTagID = xhr.id;
            ctrl.setPortable('nameTagID', ctrl.nameTagID);
            ctrl.hasIDLogo.url = 'http://localhost:8000/nametag/' + ctrl.nameTagID + '/logo';
            ctrl.hasIDBackgroundImageOptions.url = 'http://localhost:8000/nametag/' + ctrl.nameTagID + '/bg';
            ctrl.hasIDCsvUploader.url = 'http://localhost:8000/nametag/' + ctrl.nameTagID + '/csv';
            ctrl.updateQty();
            ctrl.saveText();
            $('#uploader').modal('hide');
        }
    };

    ctrl.noIDLogo = {
        url: 'http://localhost:8000/image-color',
        maxFilesize: '10',
        acceptedFiles: 'image/jpeg, images/jpg, image/png'
    };
   ctrl.hasIDLogo = {
        url: 'http://localhost:8000/nametag/' + ctrl.nameTagID + '/logo',
        maxFilesize: '10',
        acceptedFiles: 'image/jpeg, images/jpg, image/png'
    };

    ctrl.noIDBackgroundImageOptions = {
        url: 'http://imagemagic.webrevllc.com/bg',
        maxFilesize: '10',
        acceptedFiles: 'image/jpeg, images/jpg, image/png'
    };
    ctrl.hasIDBackgroundImageOptions = {
        url: 'http://localhost:8000/nametag/' + ctrl.nameTagID + '/bg',
        maxFilesize: '10',
        acceptedFiles: 'image/jpeg, images/jpg, image/png'
    };

    ctrl.noIDCsvUploader = {
        url: 'http://localhost:8000/csvUploader',
        maxFilesize: '10'
    };

    ctrl.hasIDCsvUploader = {
        url:'http://localhost:8000/nametag/' + ctrl.nameTagID + '/csv' ,
        maxFilesize: '10'
    };

    ctrl.updateQty = function () {
        if (ctrl.qty > 1000) {
            ctrl.qty = 1000;
            $scope.$apply();
        }
        // if (ctrl.currentTag.length >= ctrl.qty) {
        //     var index = ctrl.qty - 1;
        //     console.log(index);
        //     ctrl.selectTag(ctrl.myNametag.names[index]);
        // }

        while (ctrl.myNametag.names.length < ctrl.qty) {
            ctrl.myNametag.names.push([]);
        }
        if(ctrl.myNametag.names.length > ctrl.qty){
            ctrl.myNametag.names.pop();
        }
    };

    ctrl.addTag = function(){
      ctrl.qty = ctrl.qty + 1;
      ctrl.updateQty();
    };
    ctrl.selectTag = function (tag) {
        ctrl.currentTag = tag;
    };

    ctrl.currentTag = ctrl.myNametag.names[0];

    ctrl.nextTag = function (increment) {
        ctrl.currentTag = ctrl.myNametag.names[ctrl.myNametag.names.indexOf(ctrl.currentTag) + increment];
    };

    ctrl.saveText = function(){
        ctrl.setPortable('tags', ctrl.myNametag.names);
        ctrl.setPortable('numberLines', ctrl.linesSelected);
    };

    ctrl.calculateVariationID = function () {
        if (ctrl.config.selectedMaterial.urllabel == "Plastic" && ctrl.selectedPrint == "ENGRAVED") {
            return 5002
        } else if (ctrl.config.selectedMaterial.urllabel == "Plastic" && ctrl.selectedPrint == "PRINTED") {
            return 5003
        } else if (ctrl.config.selectedMaterial.urllabel == "Metal") {
            return 5004
        } else if (ctrl.config.selectedMaterial.urllabel == "Wood" && ctrl.selectedPrint == "ENGRAVED") {
            return 5005
        }
    };

    ctrl.sender = function () {
        jQuery.ajax({
            url: ctrl.nameTagID ? 'http://localhost:8000/nametag/' + ctrl.nameTagID + '/fullPost' : 'http://localhost:8000/fullPost',
            type: 'POST',
            dataType: 'JSON',
            data: {
                portable: JSON.stringify(ctrl.portable)
            }
        }).done(
            // jQuery.ajax({
            //     url: thescript.ajax_url,
            //     type: 'post',
            //     data: {
            //         action: 'myajax',
            //         quantity: ctrl.qty,
            //         variation_id: ctrl.calculateVariationID(),
            //         material: ctrl.config.selectedMaterial.urllabel,
            //         print: ctrl.selectedPrint == "PRINTED" ? "Printed" : "Engraved",
            //         color: ctrl.config.selectedColor.label,
            //         size: ctrl.config.selectedSize.urllabel,
            //         corner: ctrl.config.selectedCorner.urllabel
            //     }
            // }).done(function (response) {
            //     if (response.error != 'undefined' && response.error) { //some kind of error processing or just redirect to link
            //         // might be a good idea to link to the single product page in case JS is disabled
            //         return true;
            //     } else {
            //         window.location.href = 'http://5.254.127.105/~dev/providenceengraving/cart/';
            //     }
            // })
        )
    };

    ctrl.ovalChecked = function(){
        $("#ovalModal").modal('show');
    };
    ctrl.storageIsPrinted = function(config){
        if(!_.isUndefined(config.selectedPrintOption)){
            return config.selectedPrintOption.label == "Printed" || config.selectedPrintOption == 0;
        }else{
            console.log('caught');
            console.log(config);
            // localStorageService.clearAll();
            // location.reload();
        }

    };

    ctrl.shouldIReload = function(){
        return localStorageService.get('ID') != ctrl.pId
    };

    ctrl.clearData = function(){
        localStorageService.clearAll();
        location.reload();
    };

    if (ctrl.shouldIReload()) {
            $http.get("/nametag/" + ctrl.pId + "/data").then(function(data){
                ctrl.data = data.data;
                localStorageService.add('config', ctrl.data);
                localStorageService.set('ID', ctrl.pId);
                location.reload();
            });


    } else {
        var config = localStorageService.get('config');
        if(!_.isUndefined(config) ){
            ctrl.config.selectedMaterial = config.selectedMaterial;
            ctrl.selectedMaterial = config.selectedMaterial;
            ctrl.config.selectedCorner = config.selectedCorner;
            ctrl.config.selectedCorner = ctrl.corners[_.indexOf(ctrl.corners, _.findWhere(ctrl.corners, {label: config.selectedCorner.label}) )] ;
            ctrl.config.selectedSize = ctrl.sizes[_.indexOf(ctrl.sizes, _.findWhere(ctrl.sizes, {label: config.selectedSize.label}) )];
            ctrl.selectedBacking = config.selectedBacking;

            ctrl.portable = {};
            ctrl.portable.selectedMaterial = config.selectedMaterial;
            ctrl.portable.selectedColor = config.selectedColor;
            ctrl.portable.selectedCorner = config.selectedCorner;
            ctrl.portable.selectedSize = config.selectedSize;
            ctrl.portable.selectedBacking = config.selectedBacking;

            if(config.logoUrl){
                ctrl.hasImage = true;
                ctrl.newLogo = true;
                ctrl.logoUrl = config.logoUrl;
                ctrl.portable.logoUrl = config.logoUrl;
                ctrl.portable.colorUrl = config.colorUrl;
                ctrl.portable.blackUrl = config.blackUrl;
                ctrl.myNametag.logo.scaleWidth = config.scaleWidth;
                ctrl.portable.scaleWidth = config.scaleWidth;
                ctrl.myNametag.logo.height = config.logoHeight;
                ctrl.portable.logoHeight = config.logoHeight;
                ctrl.toggleHAlignment(config.logoHorizontalAlignment);
                ctrl.toggleVAlignment(config.logoVerticalAlignment);

                ctrl.portable.nameTagID = config.nameTagID;
                ctrl.hasIDLogo.url = 'http://localhost:8000/nametag/' + ctrl.nameTagID + '/logo';
                ctrl.hasIDBackgroundImageOptions.url = 'http://localhost:8000/nametag/' + ctrl.nameTagID + '/bg';
            }
            if(config.isFullBleed == true){
                ctrl.isFullBleed = true;
                ctrl.portable.isFullBleed = true ;
            }

            if(config.nameTagID){
                ctrl.nameTagID = config.nameTagID;
                ctrl.portable.nameTagID = config.nameTagID;
            }

            if(config.backgroundColor){
                ctrl.myNametag.background.color = config.backgroundColor;
                ctrl.portable.backgroundColor = config.backgroundColor;
            }

            if(config.backgroundURL){
                ctrl.newBG = true;
                ctrl.myNametag.background.image = config.backgroundURL;
                ctrl.myNametag.background.path = '';
                ctrl.portable.backgroundURL = config.backgroundURL;
            }

            if(config.tags){
                ctrl.myNametag.names = config.tags;
                ctrl.qty = config.tags.length;
                ctrl.currentTag = ctrl.myNametag.names[0];
                ctrl.linesSelected = config.numberLines;
                ctrl.setPortable('numberLines', ctrl.linesSelected);
                ctrl.setPortable('tags', ctrl.myNametag.names);
            }
            switch(config.selectedMaterial.value){
                case 'plastic':
                    if(ctrl.storageIsPrinted(config)){
                        ctrl.config.selectedColor =  ctrl.printOptions.plastic.printed.colors[_.indexOf(ctrl.printOptions.plastic.printed.colors, _.findWhere(ctrl.printOptions.plastic.printed.colors, {label: config.selectedColor.label}) )];
                        ctrl.selectedPrintOption = 0;
                        ctrl.selectedMaterial = 0;
                        ctrl.config.selectedPrintOption = provConfig.printOptions['plastic']['printed'];
                    }else{
                        ctrl.config.selectedColor =  ctrl.printOptions.plastic.engraved.colors[_.indexOf(ctrl.printOptions.plastic.engraved.colors, _.findWhere(ctrl.printOptions.plastic.engraved.colors, {label: config.selectedColor.label}) )];
                        ctrl.selectedPrintOption = 1;
                        ctrl.selectedMaterial = 0;
                        ctrl.config.selectedPrintOption = provConfig.printOptions['plastic']['engraved'];
                    }
                    break;
                case 'metal':
                    ctrl.config.selectedColor =  ctrl.printOptions.metal.engraved.colors[_.indexOf(ctrl.printOptions.metal.engraved.colors, _.findWhere(ctrl.printOptions.metal.engraved.colors, {label: config.selectedColor.label}) )];
                    ctrl.selectedPrintOption = 1;
                    ctrl.selectedMaterial = 1;
                    ctrl.config.selectedPrintOption = provConfig.printOptions['metal']['engraved'];
                    break;

                case 'wood':
                    ctrl.config.selectedColor =  ctrl.printOptions.wood.engraved.colors[_.indexOf(ctrl.printOptions.wood.engraved.colors, _.findWhere(ctrl.printOptions.wood.engraved.colors, {label: config.selectedColor.label}) )];
                    ctrl.selectedPrintOption = 1;
                    ctrl.selectedMaterial = 2;
                    ctrl.config.selectedPrintOption = provConfig.printOptions['wood']['engraved'];
                    break;
            }
            ctrl.portable.selectedPrintOption = ctrl.selectedPrintOption;
            localStorageService.set('config', ctrl.portable);
            console.log(ctrl.portable);
        }else{
            localStorageService.clearAll();
            location.reload();
        }
    }
});
