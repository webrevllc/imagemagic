<?php

namespace App\Http\Controllers;

use App\Nametag;
use Illuminate\Http\Request;

class NametagController extends Controller
{
    public function getLogo(Nametag $nametag)
    {
        return $nametag->logo_url;
    }

    public function jsonPost(Request $request, $id)
    {
        $nametag = Nametag::find($id);
        $nametag->data = $request->input('portable');
        $nametag->save();

        return $nametag->id;
    }

    public function show($id)
    {
        $nametag = Nametag::find($id);
        return view('nametag', compact('nametag'));
    }

    public function getData($id)
    {
        $nametag = Nametag::find($id);
        return $nametag->data;
    }

    public function index()
    {
        $nametags = Nametag::orderby('id', 'desc')->get();
        return view('nametags', compact('nametags'));
    }
}
