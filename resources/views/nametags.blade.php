@extends('layouts.app')

@section('content')

   <div class="container">
       <table class="table table-bordered">
           <thead>
                <tr>
                    <th>ID</th>
                    <th>Created</th>
                    <th>Updated</th>
                    <th>Logo</th>
                    <th>Background</th>
                    <th>CSV/Excel</th>
                </tr>
           </thead>
           <tbody>
           @foreach($nametags as $nametag)
               @if($nametag->data != null)
                   <tr>
                       <td><a href="/nametag/{{$nametag->id}}">{{$nametag->id}}</a></td>
                       <td>{{$nametag->created_at}}</td>
                       <td>{{$nametag->updated_at}}</td>
                       <td>
                           @if($nametag->logo_url != '')
                               <a class="btn btn-info btn-block" href="{{$nametag->logo_url}}"><i class="fa fa-download"></i> Logo</a>
                           @endif
                       </td>
                       <td>
                           @if($nametag->background_url != '')
                               <a class="btn btn-info btn-block" href="{{$nametag->background_url}}"><i class="fa fa-download"></i> Background</a>
                           @endif
                       </td>
                       <td>
                           @if($nametag->csv_url != '')
                               <a class="btn btn-info btn-block" href="{{$nametag->csv_url}}"><i class="fa fa-download"></i> CSV</a>
                           @endif
                       </td>

                   </tr>
               @endif
           @endforeach
           </tbody>
       </table>

   </div>

@endsection