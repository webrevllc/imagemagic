@extends('layouts.app')

@section('content')

    <div class="container" style="margin-bottom:0px;">
        <a href="/nametags" class="btn btn-danger pull-right">View All Nametags</a>
        <div ng-app="providence">

            <div ng-controller="svgFront as ctrl">

                <h1>Order #: {{$nametag->id}}</h1>
                <a href="#" ng-click="ctrl.clearData()">Reset Data</a>
                <div class="row" >
                    <div class="col-md-2" >
                        <h3>Downloads:</h3>
                        @if($nametag->logo_url != '')
                            <a class="btn btn-info btn-block" href="{{$nametag->logo_url}}"><i class="fa fa-download"></i> Logo</a>
                        @endif
                        @if($nametag->background_url != '')
                            <a class="btn btn-info btn-block" href="{{$nametag->background_url}}"><i class="fa fa-download"></i> Background</a>
                        @endif
                        @if($nametag->csv_url != '')
                            <a class="btn btn-info btn-block" href="{{$nametag->csv_url}}"><i class="fa fa-download"></i> CSV</a>
                        @endif


                    </div>
                    <div class="col-md-6" >
                        <ng-include id="svgmyNametag" src="'/svg/svg.html'" ></ng-include>
                        <div class="text-center more-margin-left preview-bottom">
                            <span ng-click="ctrl.nextTag(-1)" ng-hide="ctrl.myNametag.names.indexOf(ctrl.currentTag) == 0"><i class="fa fa-arrow-left"></i></span>
                            <span class="tag-number">
								<span class="label-nametag-number">Tag <%(ctrl.myNametag.names.indexOf(ctrl.currentTag) + 1) || 1%></span>
								<span class="label-nametag-total-qty">of <%ctrl.qty%></span>
							</span>
                            <span ng-click="ctrl.nextTag(1)" ng-hide="ctrl.myNametag.names.indexOf(ctrl.currentTag)+1 == ctrl.qty"><i class="fa fa-arrow-right"></i></span>
                        </div>
                    </div>
                    <div class="col-md-4 pricing">

                        <h4 class="text-center">Options Selected</h4>
                        <h5>Qty: <% ctrl.qty %></h5>
                        <table class="table table-condensed table-borderless">
                            <tr>
                                <td><%ctrl.config.selectedMaterial.description%></td>
                                <td class="text-right"><%ctrl.config.selectedMaterial.price | currency%></td>
                            </tr>
                            <tr>
                                <td><%ctrl.config.selectedPrintOption.label%></td>
                                <td class="text-right"><%ctrl.config.selectedPrintOption.label == "Printed" ? ctrl.config.selectedMaterial.printPrice : ctrl.config.selectedMaterial.engravePrice | currency%></td>
                            </tr>
                            <tr>
                                <td><%ctrl.config.selectedColor.label%></td>
                                <td class="text-right"><%ctrl.config.selectedColor.price | currency%></td>
                            </tr>
                            <tr>
                                <td><%ctrl.config.selectedSize.label%></td>
                                <td class="text-right"><%0 | currency%></td>
                            </tr>
                            <tr ng-if="ctrl.linesSelected > 0">
                                <td><%ctrl.linesSelected%> Lines</td>
                                <td class="text-right"><%ctrl.config.numlines[ctrl.linesSelected].price | currency%></td>
                            </tr>
                            <tr ng-if="ctrl.hasImage">
                                <td>Logo</td>
                                <td class="text-right"><%0 | currency%></td>
                            </tr>
                            <tr ng-if="ctrl.selectedBacking.url != 'none'">
                                <td><%ctrl.selectedBacking.label%></td>
                                <td class="text-right"><%ctrl.selectedBacking.price | currency%></td>
                            </tr>

                        </table>
                        <hr style="border-top: 1px solid #000;">
                        <table class="table table-borderless">
                            <tr>
                                <td><b>Total:</b></td>
                                <td class="text-right"><b><% ctrl.qty * (ctrl.config.selectedMaterial.price
                                        + (ctrl.config.selectedPrintOption.label == 'Printed' ? ctrl.config.selectedMaterial.printPrice : ctrl.config.selectedMaterial.engravePrice)
                                        + ctrl.config.selectedColor.price
                                        + ctrl.selectedBacking.price
                                        + ctrl.config.numlines[ctrl.linesSelected].price) | currency%></b></td>
                            </tr>
                        </table>


                    </div>
                </div>


            </div>
        </div>
    </div>



@section('footer_scripts')
    <script type="text/javascript" src="/js/underscore.js?2"></script>
    <script type="text/javascript" src="/js/localStorage.js?2"></script>
    <script type="text/javascript" src="/js/script.js?2"> </script>
    <script type="text/javascript" src="/js/provConfig.js?2"></script>
@endsection

@endsection