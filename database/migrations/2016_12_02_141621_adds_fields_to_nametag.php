<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddsFieldsToNametag extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('nametags', function(Blueprint $table){
           $table->string('gold_url');
           $table->string('yellow_url');
           $table->string('red_url');
           $table->string('blue_url');
           $table->string('black_url');
           $table->string('silver_url');
           $table->string('white_url');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
