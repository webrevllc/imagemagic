<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nametag extends Model
{
    protected $table = 'nametags';
    protected $fillable = ['logo_url', 'background_url', 'csv_url', 'price', 'qty'];

}
